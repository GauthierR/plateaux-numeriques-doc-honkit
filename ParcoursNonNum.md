# Parcours non numériques

----

#### Résumé
Note de Synthèse de deux demi-journées d'immersion dans les dispositifs France Service du Cauvaldor (Lot).

#### Dernière édition
07/2022

----

Notre enquête de terrain visait à comprendre l'implantation des maisons et bus france services sur la communauté de commune dont dépend Rocamadour ([Cauvaldor](https://www.cauvaldor.fr/): Causses et Vallée de la Dordogne). Nous avons également mis l'accent sur les dispositifs de communication mis en place à l'échelle nationale et locale dans le but de voir quelle est ou pourrait-être la responsabilité des mairies dans la transmission d'information relatives à ces services.

Nous avons rencontré deux agents de la Maison France Services de Gramat dans leurs locaux. Nous nous sommes également rendus à Puybrun, sur l'un des lieux de permanence du "car Cauvaldor" (le bus France Service de l'intercom) où deux agents ainsi que la coordinatrice du dispositif France Services sur le Cauvaldor nous ont accueillis. Nous avons également eu l'occasion de nous entretenir avec l'une des bénéficiaires des services de ce Car.

## Résumé & Éléments d'attention pour notre projet

Le dispositif France Service sur le territoire Cauvaldor semble rencontrer du succès auprès des administés. L'offre de services est modulée par les agents et les équipes coordinatrices du territoire afin de correspondre aux problématiques recontrées par les publics qui se rendent sur les lieux de permanence.

Les principaux points d'amélioration qui ont été révélés par notre terrain sur place sont les suivants :
* La **communication** disponible à l'échelle nationale ne correspond pas aux besoins et n'est pas assez spécifique et auto-sufisante pour être diffusée aux administrés en tant que telle.
* La **ligne directe par téléphone** avec les partenaires publics ne fonctionne pas toujours (manifestement car les agents de certains services publics ont délaissé cette ligne car elle représentait trop de travail).
* Les **informations de contact** et de localisation diffusées via les sites des partenaires publics (ex: la CAF) sont parfois fausses ou obsolètes. Cela donne lieu à des erreurs de la part des administrés.
* Le service proposé par ces structures est parfois **mal compris** des administrés, qui confondent ces lieux avec les partenaires public qu'ils relaient.
* Le lien (nécessaire) à tisser avec **le personnel des mairies** du territoire est parfois fastidieux pour les agents des maisons france services, qui ne se déplacent pas contrairement au bus.
* La **diffusion des flyers et communication papier** par les agents de la maison france service pose plus question que pour les agents du bus, qui sillonnent le territoire et peuvent déposer des prospectus et affiches dans les mairies.
* La question de la communication auprès des personnes qui ne sont **pas au courant de leurs droits** est à creuser.
* L'attente des administré.es est plutôt d'avoir **une personne qui "fait pour eux"**, plutôt que d'apprendre et de s'autonomiser sur ces démarche au niveau des savoir-faires. Il y a des attentes en terme de médiation numérique qui ne sont pas atteintes.

## Qu'est-ce que France Services?

Les agences France services ont vocation à sensibiliser les usagers aux enjeux du numérique et à les accompagner dans leurs démarches administratives en ligne sur des lieux de permanence [Lien vers le site France Services](https://www.cohesion-territoires.gouv.fr/france-services).

Les bus (ou camping car) France services désignet des dispositifs où : "les agents sont installés temporairement dans des zones prédéfinies et facilement accessibles (à proximité des mairies, marchés, centres commerciaux...). Un calendrier des permanences et passages du véhicule est communiqué à l’ensemble des habitants de la zone." [Lien](https://www.banquedesterritoires.fr/programme-france-services)

![Structure France Services](ParcoursNonNum-structurefs.jpg)

### L'implantation de France Service dans le Cauvaldor

France Service est dispositif national visant à apporter un accompagnement aux démarches administratives de proximité. Les attendus d'implantation sur le territoire sont que les administrés puissent aller à la rencontre d'agents France Service à moins de 30 minutes de chez eux.

Dans le Lot, le déploiement du dispositif a été très efficace, puisque le maillage du territoire permet aux habtiants de trouver un relais à moins de 10 minutes de chez eux. La raison évoquée par les agents est le fait d’être sur un territoire très rural, et aussi très vaste, impliquant la mise en place d’un maillage plus conséquent.

Il y a 5 Maisons France Services dans le Cauvaldor (77 communes) et le Car Cauvaldor sillonne 22 communes.

![Maisons France Services Lot](ParcoursNonNum-carte.jpg)

#### Relations tissées avec les élus et acteurs locaux

Les dispositifs France Services ont tissé des partenariats avec les associations locales comme par exemple avec les Restos du Cœur, les Missions Locales et les soupes populaires, et se redirigent des administrés ainsi. Ils peuvent également distribuer des bons alimentaires.

Ils sont aussi en lien avec des assistantes sociales pour certaines demandes nécessitant un accompagnement plus poussé sur des questions sociales (« tout accompagnement social », « tout ce qui est financier», demandes de RSA par ex). Les AS ont 3 lieux de permanence sur Cauvaldor, et se déplacent également à domicile si besoin. Les agents parlent de complémentarité avec les AS.

Le vice-président de la communauté de communes est très en phase avec la philosophie de France Services, ce qui laisse beaucoup de liberté d’action aux agents et à la coordinatrice. Ils ne rencontrent pas énormément de freins à ce niveau, et semblent vraiment libres de moduler leur service aux besoins exprimés par les administrés de leur territoire. Les territoires et les publics sont très différents, ce qui rend cette souplesse nécessaire selon la coordinatrice pour « renforcer l’offre selon les besoins ».

Les mairies redirigent les administrés vers Cauvaldor services, et acceptent aussi à l’inverse de recevoir des sollicitation des agents dans le cas où une personne se soit tournée vers le car pour un renseignement concernant la vie communale. Les mairies accueillent également les administrés qui voudraient scanner des documents à la suite d’un rdv, ou avant un rdv. Il nous a été rapporté que certaines secrétaires de mairies ont eu peur au lancement du service que le car leur « vole leur travail », mais sont finalement contentes du service et entretiennent de bonnes relations avec le car.

Des réunions ont été organisées entre les secrétaires de mairie des villages dans lesquels passe le car, et ses agents. Les agents disent avoir de bonnes relations avec le personnel de mairie, nous avons d’ailleurs pu l’observer sur place, quand l’imprimante du car a cessé de fonctionner et qu’une agent est spontanément allée à la mairie avec la personne qu’elle recevait en entretien afin d’utiliser leur imprimante.

#### Flexibilité d'organisation

Globalement, l'impression est que les agents comme les différents services (coordination, communication) ont une bonne marge de manoeuvre pour mettre en place les partenariats et méthodes de fonctionnement qui conviennent aux besoins rencontrés sur leur territoire.C'est un constat qui devrait être vérifié sur d'autres territoires, mais après visionnage d'une interview de Joël Giraud, le secrétaire d'état en charge de la ruralité à l'origine du dispositif, le discours est de "faire confiance aux collectivités locales".

^Le dispositif France Service bénéficie largement des réseaux et connaissances des communautés de communes et des agents à l'échelle locale. Ils reforcent l'offre de services nationale avec l'offre disponible sur leur territoire, ce qui permet aux administrés de trouver de nouvelles structures disponibles pour les aider près de chez eux facilement.

Quelques autres exemples d'adaptation:

* Pendant le covid, les agents se sont rendus compte qu’ils pouvaient dépanner à distance par téléphone. Depuis, les agents sont donc également disponibles pour des rdv par téléphone, ils ont tendance à appeler les administrés leur ayant fait part de difficultés entre deux entretiens en physique si personne d’autre ne se présente, car ils ne viennent que toutes les deux semaines dans un même village.

* La stratégie de communication a été largement réadaptée avec l'ajout de nouvelles informations (voir paragraphe dédié plus bas). On remarque néanmoins qu'ils ont rencontré des limites lées au non respect de la charte graphique nationale sur les documents produits par le service communication du Cauvaldor.

À ce jour, ils voient déjà que l’implantation du dispositif a reconfiguré les usages au point que « ça serait très compliqué de supprimer le car », remarque confirmée par la dame qui attendait pour un rendez-vous : « J’espère qu’il va rester très longtemps ».

#### France Service VS les autres acteurs sociaux / publics sur le territoire

![Infrastructure d'acteurs FS Caulvador](ParcoursNonNum-acteurs.jpg)

Liens pour aller plus loin:
* [Description de France Services sur le site de Cauvaldor](https://www.cauvaldor.fr/famille-et-solidarite/france-services/)

## Communication

### Communication Mise en Place à l'échelle nationale

* Des flyers et de l’affichage. Kit de com départementale du site France Service. Les flyers sont arrivés il y a 2 semaines alors que la maison existe depuis 2 ans. Les agents de la maison France services n’ont pas de créneau pour aller distribuer les affiches.
* Les sites partenaires qui redirigent vers eux

![Flyers](ParcoursNonNum-flyers.png)

Les administrés essaient d’effectuer leur démarche seuls mais n’y arrivent pas et sont redirigés par le site du service vers le relai le plus proche d’eux sur le territoire. Parfois l’information sur le site en question (ex : la CAF) est erronée, ce qui fait que les gens n’ont pas les bonnes coordonnées (adresse et horaires contradictoires selon les sites et pas possible de les corriger pour les agents). C’est aussi source de confusion pour les usagers qui pensent se rendre à la CAF alors qu’ils arrivent dans une maison France Services qui ne leur permet pas de faire les mêmes choses.

Remarque de la dame qui attendait pour un rdv : « Ils devraient dire sur le site de la CAF que le car existe ».

Expérience personnelle (Anaëlle):
* J'ai pas réussi à trouver l'info sur le site de la caf
* Sur le site de la CPAM, l'information se trouve dans les pages de contact, accessibles dans le cas où la personne ne trouve pas de réponse à sa questions dans les contenus du site.
* Selon moi, l'information est a priori difficilement accessible sur les sites nationaux, à explorer plus en détail

Les partenaires (l'exemple donné par les agents pour ce cas là est l'UDAF) peuvent également rediriger les administrés à la suite d'un entretien (par exemple un appel téléphonique). Il y a également des spots à la TV (diffusés avant les bulletins météo). Le car en lui-même est le meilleur outil de communication pour France Services. Il attire l’œil, les gens sont curieux et viennent demander aux agents quel est le rôle du véhicule. Ils le font également venir sur les marchés des communes où une maison FS est implantée afin de mieux communiquer aux habitants son existence. Nous avons parlé à une habitante d’un village voisin, venue à Puybrun à l’origine pour faire ses courses, elle a décidé de s’arrêter afin de prendre des nouvelles des démarches qu’elles avait entamées avec eux à l’occasion d’un rdv précédent. Elle avait découvert trois semaines auparavant le car à l’endroit où elle récupère son fils au bus après l’école. Ça l’avait intriguée et elle s’était donc renseignée sur les services proposés et a décidé de se faire aider pour une démarche auprès de la CAF, l’organisme physique le plus proche étant à Cahors, à 1h de route. Avant cela, elle effectuait ses démarches seule, mais cela lui prenait beaucoup de temps (notamment parce qu’elle passait beaucoup de temps à essayer de joindre les institutions tandis que les agents ont une ligne directe avec eux, et peuvent aussi répondre directement à ses questions).

### Initiatives mises en place par le Cauvaldor

À chaque fin d’année, les Maisons France Services envoient un petit article aux mairies des communes à proximité afin qu’elles le publient dans leur bulletin municipal. La coordinatrice a observé que le car est moins fréquenté dans les communes ne l’ayant pas fait.  Globalement cependant, les élus jouent le jeu, et perçoivent la valeur du dispositif. Par exemple, les mairies mettent à disposition des branchements en électricité pour le car, lui réservent une place de parking, mettent à disposition une imprimante en cas de panne de celle du car, ou leur proposent un espace permettant d’accueillir des PMR dans la mairie (ils ne peuvent pas les accueillir dans le car). Les mairies diffusent des informations concernant les structures FS de Cauvaldor via leurs sites, leurs bulletins municipaux, des tracts ou encore par l’intermédiaire des secrétaires de mairie qui redirigent les administrés. Le lieu d'implantation du car est important pour faciliter l'accès au car et le rendre plus visible. Par exemple, l'emplacement actuel du car à Rocamadour ne convient pas aux besoins car ils se garent en bas du centre ville, un lieu qui n'est pas accessible facilement par les habitants.

![Maison FS](ParcoursNonNum-maisonfs.jpg)

Des flyers et de l’affichage. Les flyers mentionnés ne sont pas la plaquette officielle, jugée trop généraliste car elle ne contient pas les lieux et horaires de passage du car. Ils ne savent pas quoi faire de ces flyers et affiches. Le service com de Cauvaldor a crée un flyer dédié, comprenant les horaires et lieux de passages, le numéro de téléphone des agents ainsi que les dates de permanence de Pôle emploi. Quand des personnes viennent en rdv, elles repartent avec ce flyer, ce qui leur permet de voir tous les sujets sur lesquels elles peuvent être aidées (ex: Une femme est venue au car en demandant aux agents si ils seraient là l’après-midi car son fils a un souci avec son contrat de travail). Le bus changeant de localisation à midi, les agents lui ont donné la plaquette avec les horaires et lieux de passage du car.

Une autre technique de distribution évoquée par une agent est plus originale : lors d’un enterrement dans l’une des communes sur lequel le car était garé, elle a mis des flyers sur toutes les voitures des personnes présentes à l’enterrement. Ça a été très efficace et ils ont reçu par la suite la visite de curieux.

La stratégie qu’ils souhaitent mettre en en place est celle d’aller directement distribuer ces tracts chez les usagers. Les agents eux-mêmes s’en chargeraient afin d’établir un premier contact avec les personnes.

Reportage où ils ont interrogé 2-3 personnes dont la maire de Puybrun, diffusion en interne (TODO demander à Céline de nous l’envoyer)

Chaque année, il y a une semaine de portes ouvertes (mi-octobre) dans les maisons France Services, qui est ouverte à tous et toutes. C’est un temps collectif de discussions autour des démarches permettant aux usagers d’échanger entre eux sur les problèmes qu’ils rencontrent. Atelier collectif avec un partenaire.

Des articles ont également été relayés dans la presse locale, un exemple [ici](https://www.ladepeche.fr/2020/10/13/maison-cauvaldor-services-les-services-publics-pres-de-chez-soi-9135274.php).

Ils se posent la question de la création d’une page Facebook. La question est de savoir qui va l’alimenter. Le service communication de Cauvaldor a beaucoup de choses à faire, et ne sont que trois pour 77 communes (une personne à temps partiel qui s’occupe des supports graphique pour toute la collectivité et deux personnes s’occupent des réseaux sociaux et de la presse). Ils n'ont donc pas encore eu le temps de finaliser leur plan de communication.

### Autres portes d'entrée des habitants

Les habitants appellent leur mairie, qui – si elle a connaissance du dispositif – les redirigera vers la Maison France Services. Selon les agents, la mise en contact avec les secrétaires de mairie tarde, ce qui ne leur permet pas de réorienter les personnes qui en auraient besoin de façon adéquate. Mathilde (agent maison FS) dit que quand elle travaillait au bus, elle « passait sa vie à faire des mails aux mairies » alors qu’à la maison, ils n’ont aucun contact avec les secrétaires de mairie.Une réunion est prévue en juin avec les mairies, assos et la préfecture pour se faire connaître.

Le bouche à oreille commence à fonctionner auprès des habitants. Il y a un problème de compréhension du service FS : "ça devrait être à la préfecture de former là dessus"

## Les différences entre la Maison France Services et le Car Cauvaldor


### Le Bus Cauvaldor

![Bus Cauvaldor](ParcoursNonNum-bus.jpg)

#### Publics

Ce sont majoritairement des personnes âgées de 65 à 80 ans, de par la démographie du territoire (petits villages à la population vieillissante). Ce sont des personnes qui sont isolées socialement, et beaucoup viennent en partie les voir uniquement dans le but de voir du monde (avec la dématérialisation, il n’y a plus personne en face de soi pour faire les démarches, le lien est coupé).

Ils rencontrent aussi des publics précaires, qui peuvent eux être hors de la tranche d’âge des 65-80ans : des familles, très peu de jeunes.

Chaque commune a sa particularité, qui fait varier les publics : à Biars, il y a l'entreprise Andros donc de l’emploi ; à Souillac, on trouve plus de précarité financière ; à Gramat, il y a beaucoup de migrants.

Ce sont aussi des personnes qui ne peuvent pas se faire aider dans leurs démarches par leur entourage. Les usagers viennent en général pour effectuer une démarche en particulier et avoir de l’aide pour débloquer la situation rapidement. Pour les démarches complexes, il y a des publics réguliers.

Le car a surtout un rôle de lien social, en plus de son aide aux démarches (accueil avec des petits biscuits fait maison, connaissances qui se reconnaissent dans le bus).

Les publics viennent à pied ou en voiture. S’ils ne sont pas disponibles le jour de la permanence dans leur commune, il arrive qu’ils se déplacent pour aller au bus dans une autre commune (ex: une habitante de Tauriac va au bus à Tauriac et Puybrun).

#### Les situations d'exclusion rencontrées

Les raisons listées par les agents (ainsi que la coordinatrice du réseau France Services de Cauvaldor):

* Pas d’ordinateur
* Pas internet
* Ne sait pas ou ne veut pas utiliser des appareils numériques ou Internet

Ils observent une différence entre des public plus jeunes qui sont plutôt à l’aise avec l’utilisation d’outils informatiques (surtout les réseaux sociaux) mais qui ont du mal à réaliser des démarches administratives, et des publics plus âgés qui savaient réaliser leurs démarches en version papier, mais n’y arrivent plus une fois le service public dé-matérialisé.

Le risque pour les administrés qui ne se présentent pas alors qu’ils en ont besoin est l’abandon de leur démarches, avec pour conséquence le risque de non-accès aux droits, conséquence de la complexité des démarches (ex: Certaines personnes n’ont plus de médecin et ne se soignent presque plus, elles ne savent pas qu’elles ont le droit à un bilan santé gratuit chaque année).

#### Démarches effectuées

Les sollicitations des bénéficiaires dépassent les services initialement proposés dans le cadre de France Services. Par exemple, une agent nous a dit avoir reçu la veille une personne voulant changer de forfait téléphonique. Parfois on vient également leur poser des questions concernant les services de mairie (comme les horaires de passage des poubelles ou le plan des sentiers de randonnée), ou de l’aide à la rédaction de documents.

Leur définition large de leur offre de services est « Tout ce qui rentre dans le service public ».

Les personnes ne comprennent pas toujours le périmètre d’action des agents, et ont tendance à les confondre avec les institutions (exemple d’un Mr qui est venu lorsqu’on était là, qui voulait refaire sa carte d’identité mais n’avait pas l’air d’avoir compris qu’il ne pouvait réaliser que la pré-demande). Pour certaines personnes, les démarches sont complètement abstraites (incompréhension du parcours : pré-demande en ligne puis passage à la mairie).

### La maison France Services de Gramat

![Pole social](ParcoursNonNum-polesocial.jpg)

#### Publics

Le administrés se rendant à la Maison France Services de Gramat ont des profils plus variés que les bénéficiaires du Car Cauvaldor (familles, jeunes, sdf, personnes étrangères, familles précaires). Ces personnes recherchent plutôt un format d’accompagnement, les personnes viennent avec un projet et ont besoin d’aide, parfois sur le long terme. Ce sont des profils qui ont souvent besoin d’un accompagnement plus social que technique. Les raisons sont multiples: la maison France Services est située dans l'ancien "Pôle Social" de Gramat (un Centre Communal d'Action Social), un bâtiment qui abrite notamment les restos du coeur, et était déjà identifié comme un lieu d'accueil pour des personnes ayant besoin d'effectuer des prestations en lien avec une situation de précarité ou de difficulté sociale. Isabelle, l'une des deux agents France Services que nous avons rencontrée à Gramat, a changé d’organisme au moment de la mise en place de la maison, elle a quitté le CCAS pour devenir agent d'accueil France Service. À ce moment là, les personnes qu’elle suivait l’ont "suivie" et ont continué leur accompagnement avec elle, mais dans le cadre de France Services. Certaines personnes sont suivies par Isabelle depuis 10 ans.

Selon les deux agents, le public admis varie selon le territoire et le lieu dans lequel la maison france services est implantée. Par exemple, certaines maisons France Service se trouvent dans des bibliothèques, ou dans des bureaux de poste donc la fréquentation et l’organisation du lieu sont différentes.

#### Les situations d'exclusion rencontrées

Les raisons pour lesquelles le gens viennent chercher de l’aide :

Les même freins d'exclusion ont été cités par les agents de la maison France services. Des personnes en situation d'Illectronisme ou n'ayant pas internet se présentent plusieurs par jour. Il y a également des personnes ne parlant pas français qui viennent se faire aider (les langues que les agents peuvent rencontrer ici sont l'afghan ou le pakistanais par exemple). Elles ont également mis l'accent sur la situation des jeunes, qui "pensent avoir des bases mais se trompent, ils sont à l’aise seulement avec les réseaux sociaux". Certaines personnes viennent également car elles ont peur de se tromper dans leurs démarches et ont besoin d'être rassurées. Certaines personnes n'ont "pas envie de faire seul" et se présentent facilement à la maison FS, tandis que d'autres n’ont « pas envie de mendier », mais se tournent vers la maison FS lorsqu’ils sont dans une impasse.

L'enjeu reste de toucher les personnes qui ne sont pas au courant de leurs droits et qui ne font donc pas la démarche de solliciter certaines aides.

#### Démarches effectuées

Exemples de services pour lesquels les personnes viennent : faire une carte grise, un dossier retraite, créer une adresse mail... Elles informent aussi sur les services Cauvaldor : urbanisme, avocats, Poste... Ces missions varient selon les partenariats locaux, différents selon les maisons : à Gramat, maison des aidants, ADIL, impôts ; ailleurs, par exemple SPIP (judiciaire / réinsertion).

Les ordinateurs en libre service sont non utilisés la plupart du temps. La mise en place d’ateliers est prévue à Gramat afin d’autonomiser les administrés sur l’utilisation de cet outil. L’idée est donc qu’ils fassent eux-même, mais aient une personne accompagnante à proximité leur permettant d’être en confiance et de les débloquer si besoin (c'est une remarque qui visait en particulier les publics jeunes). « Il y en a à Gramat qui ne s’autonomisent pas, qui attendent qu’on fasse pour eux ».Il y a aussi dix ordinateurs en libre service à la médiathèque de Gramat. Ils étaient bien utilisés jusqu’au Covid, le responsable Dominique faisait des formations, il est « très pédagogue », mais depuis le Covid ils ne sont plus utilisés. Les agents nous ont également parlé de deux belges à Alvignac : la femme fait du tricot et l’homme fait de l’initiation informatique à la salle des fêtes.


### Profil des agents & quotidien

#### Profil des agents

##### Car Cauvaldor:

**Emmanuelle** : depuis 2 mois au Car, elle était au service Technique à Cauvaldor.

**Stéphane** : depuis 2 mois au car, travaillait chez Bouygues avant.

Ils ont une formation socle d’une semaine, tout comme les agents des maisons FS. Cette formation s’organise en 2 temps : 2,5 jours socle et 2,5 jours avec les opérateurs partenaires. Puis ils ont des formations continues tout au long de l’année pour lesquelles ils sont en binômes, mais pas 2 personnes de la même structure (mélangés bus et maisons).

##### Maison France Services

**Mathilde** : travaille à la maison FS depuis janvier, a travaillé pour les cars Cauvaldor pendant 2 ans. Assistante sociale de Formation.

**Isabelle** : a travaillé pendant 10 ans à Gramat d’abord pour le CCAS (Centre Communal d’Action Sociale) puis le CIAS (Centre Intercommunal d’Action Sociale), à la Maison France Services au lancement du dispositif il y a 2 ans. Elle a une formation d’éduc spé.

Les profils des agents varient beaucoup au niveau de leurs études initiales. Ils ont une formation socle organisée par la préfecture pour rencontrer les partenaires sociaux pour lesquels ils vont faire relais auprès des publics.

#### Quotidien des agents de la maison

* Selon les périodes de l’années (en ce moment, beaucoup de sollicitations pour les impôts et les chèques énergie).
* Environ une vingtaine de démarches à 2 chaque jour.
* Normalement, l’accueil se fait sur RDV mais beaucoup de gens viennent sans RDV et elles les accueillent aussi autant qu’elles peuvent. Elles font aussi des permanences (par exemple permanence pour déclaration d’impôts) pendant lesquelles elles accueillent sans rdv. Le jeudi matin, la maison est fermée au public parce que les agents sont en formation mais il y a des gens qui viennent quand même (interrompue 3 fois la dernière fois). Les gens ne comprennent pas forcément que les agents sont sur place mais ne peuvent pas les accueillir.
* Elles ont des formations très fréquentes, c’est pourquoi les agents se divisent les formations sur le territoire, où chacun est référent d’une compétences (par ex. Mathilde est référente violences conjugales et Isabelle est référente sur les questions de précarité financière).
* Mauvaise communication entre les structures partenaires. N’ont pas de ligne directe avec certains partenaires (doivent appeler la CAF ou la CPAM avec le même numéro que nous), ce qui fait qu’elles n’arrivent pas à avancer sur un dossier, parce que si la personne n’est pas avec elles quand elles appellent, le partenaire ne veut pas leur répondre. Au début, ils répondaient assez vite par e-mail, mais maintenant plus trop « parce qu’ils se sont rendu compte que c’était pas 1 mail par semaine mais plutôt plusieurs par jour ». Et ils n’ont pas non plus de leur côté des agents à plein temps pour répondre aux mails.
* Elles ont des problèmes de connexion dans la maison FS, dès qu’il y a 4-5 personnes connectées en même temps.
* Elles dynamisent et animent la structure : si elles veulent organiser une rencontre, une fête etc, elles peuvent.


#### Quotidien des agents du bus

* Un RDV type est compté 45 minutes administrativement sur la com com. Ils peuvent cependant durer plus longtemps selon les besoins des administrés. Selon eux, un rdv dure en général, 45min dans une maison FS, et jusqu’à 1h30 dans le bus. « Ils ont le temps de prendre le temps ». Il y a des jours remplis, et d’autre moins qui laissent le temps d’avancer sur des dossiers en cours. Il peut y avoir 2 rdv en même temps, les deux espaces « bureau » dans le car peuvent être séparés par une cloison.

Exemple : quand on y était, deux monsieurs sont arrivée séparément autour de 10h10 et sont repartis autour de 11h.

Si la demande ne rentre pas dans leur champ d’action, ils redirigent vers un autre service qui saura mieux y répondre. Exemple : pour une demande concernant un contrat de travail, ils ont répondu qu’ils n’étaient pas juristes, qu’ils pouvaient lire le contrat et conseiller, puis si besoin orienter vers un spécialiste.

L’été, quand il y a beaucoup de touristes à Rocamadour, c’est parfois difficile de se frayer un chemin pour se garer.

Peu de monde à Rocamadour car ils se garent en bas, vers la mairie actuelle, mais ça devrait aller mieux quand la mairie sera en haut car ils se garerons aussi en haut.

Mathilde dit que quand elle travaillait au bus, elle « passait sa vie à faire des mails aux mairies » alors qu’à la maison, ils n’ont aucun contact avec les secrétaires de mairie.Une réunion est prévue en juin avec les mairies, assos et la préfecture pour se faire connaître.

Les bénéficiaires repartent avec un mémo sur papier, récapitulant les mots de passe et identifiants créés, doivent-ils le rapporter ensuite?

Ils ont également beaucoup de formations.