# Comment rédiger des contenus accessibles

----

#### Résumé
Cet article présente les enjeux autour de la simplification de l'information et donne des conseils pour rédiger des contenus accessibles

#### Dernière édition
06/2022

----

## Avant-propos

Cette fiche est une synthèse de la formation de Design Gouv "[Simplification de l'information aux usagers](https://www.youtube.com/watch?v=7scX1FO_FQM&list=WL&index=65)" donnée par Camille Rozier (DITP)

Une fois récapitulative a été réalisé par Pierre La Rocca et est disponible [ici](https://nuage.plateaux-numeriques.fr/s/YrbLZEZWYSjP6pX).

## Les enjeux

![Aurélie à Lalouvesc](img/RedigerContenu-aurelie.jpg)

Quand on est usager d'un service public, on réalise des démarches et très souvent, en les réalisant, on n'a accès qu'a des supports d'informations, des interfaces en lignes, des formulaires. **Parfois, l'usager n'a une relation avec l'administration que par ces interfaces et ces supports**. Ces supports sont parfois/souvent négligés, or c'est un élément crucial.

Simplifier l'information, c'est rendre l'information, son contenu et sa forme plus adaptée aux usagers. **Ce n'est pas** appauvrir la langue, **ce n'est pas** retirer de l'information, **ce n'est pas** une solution à tous les enjeux de complexité. C'est un travail à la fois sur le contenu (le fond) et le contenant (la forme).

Il s'agit de prendre en compte les caractéristiques cognitives des usagers, faire preuve d'**empathie**, en particulier auprès des allophones, publics jeunes, personnes agées, RQTH, d'un public en manque de temps, avec une attention limitée, une fatigue cognitive, des biais cognitifs.

Cet exercice nécessite de prêter attention à notre fonctionnement cognitif. Notre cerveau fonctionne de manière approximative, par raccourci de penser (des heuristiques) et des interprétations automatiques. C'est un système optimal pour traiter beaucoup d'information, d'agir vite, de donner du sens et de les mémoriser.

### Quelques exemples de biais cognitifs

#### Le biais du rédacteur, la malédiction de la connaissance.

Quand on a accès à une connaissance, on est naturellement confiant que les autres vont la comprendre facilement. Si je connais quelque chose par cœur, en délivrant de l'information, j'ai l'impression que cette information est suffisante. Sauf qu'en tant que novice, ça peut être très compliqué.

#### L'aversion au risque

Plus on met d'info, mieux c'est. On a une réticence à prendre des risques.

#### Le biais du statu quo

"On a toujours fait comme ça".

## Aller à l'essentiel

Posez vous les bonnes questions :
* Quelle est l'information principale que je souhaite délivrer ?
* Quel est l'objectif de mon document ?
* De quelle information l'usager a t il besoin ?

Tenez vous en message principal :
* Supprimer les informations non essentielles
* Soyez direct

Préférez 2 niveaux d'information et quand cela est possible, faites des renvois en bas de page, ou reportez en annexe les textes de foi et détails administratifs.

Utilisez un language simple : 
* Proroguer -> prolonger
* Stipuler -> indiquer
* Au titre de l'article 37 -> selon l'article 37
* Eviter les sigles et les acronymes, définissez les entre parathensès
* Expliciter les termes compliqués qui ne peuvent être remplacer (créancier, personne morale, raison sociale). Ce ne sont pas des mots que l'on apprend à l'école, on ne les rencontre pas couramment.

Si besoin, voir le lexique administratif [ici](https://www.loiret.gouv.fr/content/download/10885/69552/file/LEXIQUE.pdf).

Utiliser plutôt les verbes que les noms :
* Pour l'optimisation -> pour optimiser
* Eviter les adverbes en -ment : Mensuellement -> Tous les moins, Préalablement -> avant

Choisissez un mot par concept - évitez l'emploi de synonyme ou de mots voisins :
* Veuillez effectuer votre *demande* lors du dépot du dossier de création. Votre *requete* sera traitée au plus vite -> Veuillez effectuer votre *demande* lors du dépot du dossier de création. Votre *demande* sera traitée au plus vite.

Evitez les mots polysémiques qui ont plusieurs sens.

Donnez des exemples pour illustrer une notion ou un concept. 

Construisez des phrases simples à la forme active :
* Seront également acceptées les demandes envoyées par courrier électronique -> Les demandes envoyées par courrier électronique seront également acceptées.
* L'obtention du permis est soumise à la production d'une pièce justificative -> pour obtenir le permis, vous devez nous envoyer un document justificatif.

## Organisez l'information avec logique

Essayez de construire l'information avec vos usagers :
* Regrouper les informations similaires
* Une idée un paragraphe
* Faites des sous parties
* Nommes les différentes parties avec des titres explicites

## Personnalisez et soignez la relation avec le lecteur

* Privilégiez une relation de proximité
* Ton positif
	* évitez les tons autoritaires, anxiogènes ou culpabilisants
* Pédagogie et transparence
	* Eviter les postures moralisantes

## Illustrez l'information

[à compléter]