# Informations admnistratives de l'association

## Déclaration à la préfecture de police

* 4 novembre 2021
* Annonce n°1093 au JOAFE

## Répertoire des assocations et des entreprises

* SIREN : 905 238 457
* SIRET : 905 238 457 00014
* Dénomination : Plateaux Numériques
* Catégorie juridique : 9220 Association déclarée
* N° RNA : W751262679
* N° APE : 63.99Z
* ESS : oui

## Dirigeants

* Gauthier Roussilhe, co-président
* Timothée Goguely, co-président
* Michel Roussilhe, trésorier

## Comptabilité

* Cabinet Philibert Adebiaye, expert-comptable

## Statuts

* Statuts de l'association Plateaux Numériques

## Procès verbaux

* Procès-verbal de l'assemblée constitutive du 5 octobre 2021

## Banque

* Credit mutuel
* FR76 1027 8060 3700 0210 0510 179
* CMCIFR2A
* CCM Paris 15 Champ de Mars Entrepreneurs, 70 rue des entrepreneurs, 75015 Paris