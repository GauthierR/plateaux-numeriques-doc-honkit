# Présentation de l'interface d'édition

----

#### Résumé
Cette page présente les différentes possibilités de l'interface d'édition de sites web. Une liste de questions fréquentes est présente en bas de page.

#### Dernière édition
06/2022

----

## Liens d'accès

Pour accèder à l'interface d'édition Osuny : https://edition.plateaux.tech

## Présentation de l'interface

### Site web

![Page principale](img/PresInterfaceEdition-pageprincipale.png)

L'accès à l'interface d'édition mène à la page "Site Web", qui fait office de page d'accueil. On y retrouve plusieurs éléments :

En haut de la page se trouve le nom du site, ici "starter-kit", et sur la droite, le lien d'accès à ce ce dernier (on parle d'URL).

**En 1**, on trouve le menu de l'interface d'édition du site. Chacun de ces liens sera explicité dans une section de cette documentation. Voici un rapide descriptif de chacun des onglets :
* **Site Web** : Page actuelle. Permet d'obtenir une vue d'ensemble des derniers contenus du site, mais aussi d'accéder à l'interface technique du site.
* **Arborescence** : Permet d'accéder à la page de création des contenus statiques du site web.
* **Actualités** : Permet d'accéder à la page de création des actualités du site web.
* **Menus**: Permet d'accéder à la page de création des menus du site web.

**En 2**, on trouve les dernières actualités créées. Il est possible d'accéder directement à l'onglet "Actualité"" en cliquant sur "Voir la liste complète", mais aussi d'en créer de nouvelles en appuyant sur "Créer". On peut également directement modifier ou supprimer une actualité récente depuis cette interface.

[Note : définir ce que fait "Nouvelle Curation"]

**En 3**, on trouve les dernières pages créées. Il est possible d'accéder directement à l'onglet "Arborescence" en cliquant sur "Voir la liste complète", mais aussi d'en créer de nouvelles en appuyant sur "Créer". On peut également directement modifier ou supprimer une actualité récente depuis cette interface.

Sur le bas de la page, le bouton "Modifier" permet d'accéder aux paramètres de configuration du site web. Ce bouton est réservé aux publics techniquement avertis. Plus d'informations sur la configuration du site web, sont disponibles sur la page de [configuration entre Osuny et Gitlab](ConfigOsunyGit.md)

### Arborescence

![Arborescence](img/PresInterfaceEdition-arborescence.png)

La page "Arborescence" contient la hiérarchie des pages de contenu du site. Les pages y sont présentées comme des dossiers.

Un dossier blanc correspond à une page sans sous-pages (comme pour la page "Mairie" dans l'illustration).

Un dossier bleu correspond à une page possédant des sous-pages, on parle aussi de page mère et de pages filles. (Dans l'illustration, la page "À propos" est mère des pages "Mentions légales", "Plan du site" et "Politique de confidentialité").

La page "Arborescence" permet les actions suivantes :

**En 1**, créer des pages de contenu en appuyant sur le bouton "Créer".

**En 2**, visualiser l'arborescence de pages actuelle.

Pour "Modifier" / "Supprimer" des pages existantes, on peut cliquer sur les boutons associés. Certaines pages, présentes par défaut dans le site, ne peuvent pas être supprimées. C'est le cas des pages "Accueil", mais aussi des pages "Mentions légales", "Plan du site" et "Politique de confidentialité".

Passer le pointeur de la souris sur une page permet d'ouvrir le dossier associé pour consulter ses sous-pages.

On peut modifier la hiérarchie existante de deux façons différentes depuis cette page :

* Pour changer l'ordre de pages d'un même niveau, sans en modifier la profondeur, il faut cliquer sur une page et la glisser / déposer plus haut ou plus bas, en fonction d'ou on souhaite la déplacer.
* Pour modifier la profondeur d'une page, il faut ouvrir le dossier de la page qui sera sa page supérieure, puis y glisser / déposer la page dont on souhaite modifier la hiérarchie.

Pour en savoir plus sur l'interface de création des pages, vous pouvez consulter l'article de [création de pages](CreerPageContenu.md)

### Actualités

![Actualités](img/PresInterfaceEdition-actualites.png)

La page "Actualités" contient l'ensemble des actualités du site.

La page "Actualités" permet les actions suivantes :

**En 1**, créer des actualités en appuyant sur le bouton "Créer".

**En 2**, consulter dans un tableau les actualités existantes. Le tableau des actualités offre les possibilités suivantes :

* Afficher les informations relatives aux actualités (Titre, Image à la une, Auteur.ice, Catégories de l'actualité et Date de publication dans le cas d'une actualité publiées).
* "Modifier" / "Supprimer" une actualité spécifique en cliquant sur les boutons associés.
* Le bouton "Filtrer le tableau" offre des options de tri diverses ainsi qu'un onglet de recherche.
* Il est possible de sélectionner une ou plusieurs actualités à l'aide de la "case à cocher" pour réaliser une publication multiple.

**En 3**, la création de catégories d'actualités, afin de renseigner sur ces dernières. Pour en savoir plus sur l'interface de création de catégories d'actualités, vous pouvez consulter l'article suivant : [Renvoi création de catégories d'actualités Osuny]

**En 4**, consulter le tableau des auteur.ices d'actualités. Ce tableau est notamment utile dans le cas où plusieurs personnes collaborent à la rédaction d'un site web.

Pour en savoir plus sur l'interface de création des actualités, vous pouvez consulter l'article de [création d'actualités](CreerActu.md)

### Menus

![Menus](img/PresInterfaceEdition-menus.png)

L'interface des menus donne accès à deux menus : Menu Principal et Menu Rapide :

* "Menu Principal" permet d'éditer le menu principal du site, il est recommandé d'y renseigner la structure de site que vous souhaitez mettre en place.

* "Menu Rapide" permet de mettre en avant certains contenus sur la page d'accueil de votre site. Nous vous recommandons d'y ajouter les pages les plus utiles ou les plus visitées.

**Attention : Ces menus sont liés au fonctionnement interne de votre site web, nous vous recommandons de ne pas modifier leurs paramètres, de ne pas les supprimer et de ne pas créer d'autres menus sous peine de perturber le fonctionnement du site.**

Pour éditer le contenu des menus, rendez-vous dans le menu de votre choix en cliquant sur le titre du menu désiré. Pour en savoir plus sur l'interface d'édition du contenu d'un menu, vous pouvez consulter l'article sur le [contenu des menus](CreerMenu.md)

