# Documentation de Plateaux Numériques

![Lalouvesc](img/ReadMe-roca.jpg)<br>
<i>Formation à la mairie de Rocamadour avec Nathalie et Timothée</i><br>

Documenter, rendre visible et accessible les informations relatives au projet Plateaux Numériques est une pratique indispensable pour sa réussite. Cette pratique répond à un double enjeu de transparence de l'action publique et de construction un patrimoine informationnel commun.

Si vous en êtes arrivé à lire cette documentation, c'est que vous savez sans doute où vous mettez les pieds. Néanmoins, pour rappel, Plateaux Numériques est un projet qui vise à accompagner les mairies de villages ou petites communes dans la conception et l'hébergement de site web utiles, accessibles, abordables, ancrés dans le territoire, à faible impact environnemental et conforme aux réglementations en cours et à venir.

Plateaux numériques est porté par une association à but non lucratif dont le rôle est de faciliter/coordonner le déploiement du service par les réseaux locaux existants, grâce au développement de l'outil, de la documentation et de la mise en réseau des acteurs.

[à compléter]

## Liste des plateaux actifs
* Lalouvesc, Ardèche, depuis le 1er avril 2021
	* Référent : équipe PN
* Rocamadour, Lot, à partir du 1er septembre 2022
	* Référent : équipe PN

## Liste des plateaux à confirmer
* villages dans le Béarn
* villages dans le Finistère

