# Animer des ateliers

----

#### Résumé
Cet article présente les enjeux autour de la simplification de l'information et donne des conseils pour rédiger des contenus accessibles

#### Dernière édition
06/2022

----

## Remarques générales

Lors des ateliers de conception, notre rôle relève davantage de la posture d'accompagnateur, de guide, d'observateur, que de la création des idées

* Il est nécessaire de laisser les acteurs locaux s'exprimer, d'être à leur écoute pour identifier leurs besoins

En tant que designer, il est parfois nécessaire de recentrer les personnes sur l'atelier, ces dernières pouvant se disperser vers les problématiques locales ou particulières du village. Si cela est nécessaire par moment, des dérives trop régulières peuvent mener à perdre de vu l'objectif du travail en cours.

Si des documents, types fascicules, semblent intéressants, il est possible de les collecter
* Penser à demander l'autorisation explicite en amont

Réaliser des pauses régulières au cours des ateliers : les personnes ne sont pas toujours formées à ce genre de pratiques. Par ailleurs, des idées peuvent émerger hors contexte, ou durant les pauses. Il est nécessaire d'y rester attentif.

## Guides des ateliers possibles

### Activité 1 : Cartographie des acteurs locaux

* Structure : Collectif
* Durée : 1 heure
* Instructions
	* Définir le millefeuille administratif (région, département, interco, etc.)
	* Demander les compétences de chacun de ces acteurs et la façon dont ils sont liés les uns aux autres

* But
	* Comprendre les acteurs locaux qui sont en jeu dans la conception du site web parce qu'ils disposent de compétences nécessaires au fonctionnement de la commune
	* Améliorer rapidement la compréhension des enjeux locaux
	* Comprendre les impacts possibles sur l'arborescence future et les fonctionnalités rattachées

![Cartographie des acteurs locaux](img/JournalTerrain-carto.jpg)


### Activité 2 : Chapeaux de Bono

![Cartographie des acteurs locaux](img/AnimAteliers-carto.jpg)

* Structure : Collectif
* Durée : 1 heure
* Instructions
	* Présenter les visions à mobiliser sous forme de chapeaux (émotions, pessimisme, optimisme...)
	* Pour chaque chapeau :
		* Donner 5 minutes de temps individuel pour que chaque personne puisse renseigner ses idées
		* Organiser une restitution collective

* But
	* Séquencer la pensée des participant·e·s en fonction de modes de penser spécifiques


### Activité 3 : Atelier Arborescence

![Cartographie des acteurs locaux](img/AnimAteliers-arbo.jpg)

* Structure : Collectif
* Durée : 2 heures
* Préparation en amont
	* Si existante, lister l'architecture actuelle du site sous forme de post-it avec découpe sections / pages
	* Sinon, proposer une architecture initiale
* Instructions
	* Présenter l'architecture de départ, en invitant les acteurs à s'en saisir pour la modifier à leur convenance
	* Commencer par regrouper les pages en fonction des thématiques abordées. Il est possible d'ajouter des pages sur de nouveaux post-it, de fusionner des contenus sur une page ou encore de changer une page de section. Les groupes de page obtenus définissent de nouvelles sections.
	* Il est possible de regrouper des sections entre elles pour mettre en place une arborescence plus profonde
	* Une fois la structure générale posée, travailler les titres des points d'accès
* But
	* Définir une arborescence de l'architecture web
	* Lister des points d'entrée

### Activité : Liste de course

* Structure : Collectif (potentiellement a posteriori)
* Durée : 1 heure
* Instructions
	* Pour chaque page et section de l'arborescence définir si le contenu existe déjà (texte, images, liens, docs, videos) ou s'il doit être produit / refait
	* Si le contenu existe déjà, doit-il être retouché ?
	* Qui produit / refait les contenus ?
* But
	* À l'issue de cet exercice vous aurez produit une liste de course pour tout le site des contenus bons en l'état, à retoucher, à produire/refaire
	* Une personne est assignée à chaque contenu à produire / refaire, la liste de course est prête