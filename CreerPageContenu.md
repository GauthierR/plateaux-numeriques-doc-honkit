# Créer une page

----

#### Résumé
Cette page présente l'interface d'édition de pages avec Osuny.

#### Dernière édition
06/2022

----

## Liens d'accès

Pour accèder à l'interface d'édition Osuny : https://edition.plateaux.tech
Puis, se rendre dans l'onglet "Arborescence".

## Présentation de l'interface

![Arborescence](img/PresInterfaceEdition-arborescence.png)

La page "Arborescence" permet les actions suivantes :

* En 1, créer des pages de contenu en appuyant sur le bouton "Créer".
* En 2, visualiser l'arborescence de pages actuelle.

Pour "Modifier" / "Supprimer" des pages existantes, on peut cliquer sur les boutons associés. Certaines pages, présentes par défaut dans le site, ne peuvent pas être supprimées. C'est le cas des pages "Accueil", mais aussi des pages "Mentions légales", "Plan du site" et "Politique de confidentialité".

Passer le pointeur de la souris sur une page permet d'ouvrir le dossier associé pour consulter ses sous-pages.

### Modifier la hiérarchie existante

On peut modifier la hiérarchie existante de deux façons différentes depuis cette page :

* Pour changer l'ordre de pages d'un même niveau, sans en modifier la profondeur, il faut cliquer sur une page et la glisser / déposer plus haut ou plus bas, en fonction d'ou on souhaite la déplacer.
* Pour modifier la profondeur d'une page, il faut ouvrir le dossier de la page qui sera sa page supérieure, puis y glisser / déposer la page dont on souhaite modifier la hiérarchie.

## Créer une page

Pour commencer, appuyer sur le bouton **Créer** pour arriver à l'interface ci-dessous.

![Page-type](img/CreerPageContenu-page.png)

L'interface de création d'une page permet de configurer la page. Elle possède 4 panneaux d'édition : Contenu, Informations, Image et SEO. Pour ajouter du contenu à la page, il faut d'abord la configurer puis l'enregistrer.

### Contenu

* Titre : Titre de la page (obligatoire)
* Nom affiché dans le fil d'Ariane : Permet de personnaliser le nom de la page dans l'arborescence de navigation.
* Chapô : Non utilisé par le thème de Plateaux Numériques
* Titre affiché dans le header : Permet de personnaliser le texte affiché dans l'onglet du navigateur.

### Informations

* Publié ? : Permet de publier la page sur le site web.
* Slug : Le code de la page est rempli automatiquement par Osuny. Sa modification est reservée aux utilisateurs avertis.
* Page Parente : Permet de placer la page créée dans la hierarchie des pages.

### Image

Permet de définir une image de couverture de la page.

* Parcourir : Choisir une image stockée localement.
* Texte Alternatif : Texte descriptif de l'image nécessaire à l'accessibilité.
* Crédit : Permet de renseigner les droits liés à l'image utilisée.
* Chercher une image : Permet de trouver une image libre d'utilisation via le service Unsplash.

### SEO

Permet de référencer la page sur les moteurs de recherche.

## Éditer le contenu d'une page

L'interface d'édition du contenu d'une page est accessible soit directement après avoir créé une page, soit en appuyant sur le bouton "Modifier" d'une page dans la hiérarchie des pages.

![Edition du contenu](img/CreerPageContenu-editionpage.png)

* Modifier : Permet d'accéder à l'interface de création d'une page
* Supprimer : Permet de supprimer la page

Dans Osuny, le contenu des pages fonctionne sous forme de blocs. Ainsi, pour ajouter du contenu, il est nécessaire de passer par le bloc de contenu approprié. Pour cela, il faut appuyer sur "Ajouter" dans la partie "Blocs de contenu", et choisir le bloc désiré.

Quelques exemples de blocs de base sont : "Chapitre" (Pour ajouter un texte structuré avec un titre), Image, Vidéo ou Tableau.

Une fois qu'un bloc est ajouté, il est possible de le modifier ou de le supprimer, en appuyant sur le bouton associé à l'action désirée.

[à compléter]


## Questions

##### Qu'est-ce que le fil d'Ariane ?

Le fil d'Ariane représente la position de l'utilisateur dans l'arborescence du site. Il est présent sur la partie haute de la page et prend la forme Accueil/page-antérieure/.../page-actuelle.