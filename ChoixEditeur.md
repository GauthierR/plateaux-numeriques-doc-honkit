# Le choix de l'éditeur

L'objectif de Plateaux Numériques est de proposer aux mairies de villages des sites web utiles, accessibles, abordables, ancrés dans le territoire et à faible impact environnemental. La publication de contenus statiques, c'est-à-dire de pages html simples, apparaît comme une solution en accord avec nos objectifs car moins coûteuse en ressources. De nombreuses solutions existent aujourd'hui pour permettre la publication de contenus statiques. Nous cherchons ici à expliquer le choix de notre éditeur de contenu.

## Les caractéristiques déterminantes

Les valeurs portées par notre service ont dans un premier temps motivé des caractéristiques fonctionnelles essentielles. Elles se répartissent en trois critères : territorial, résilience et convivialité d'usage.

### Critère territorial
Les sites et contenus produits s'ancrent dans un territoire restreint et particulier. Celui-ci est doté d'une culture propre, et ses habitants possèdent des compétences à maintenir au niveau local.

La solution doit alors posséder une interface en français, pour tenir compte des personnes éditant les contenus, mais aussi permettre de publier des contenus en plusieurs langues, afin de s'adapter aux spécificités culturelles locales, comme au Pays Basque où nous opérons actuellement.

### Critère résilience
Les sites sont produits pour s'inscrire durablement dans le temps, s'adapter aux évolutions à venir et essayer de prévenir au mieux toute forme d’obsolescence.

La solution doit alors être dotée d'une bonne communauté qui favorisera son maintien dans la durée. Il est aussi préférable que son développement soit maintenu et qu'elle ait une certaine longévité.

### Critère convivialité
L'usage des sites ne doit pas porter atteinte à l'autonomie des personnes. Il s'agit d'apporter un service et non d'asservir les utilisateurs et éditeurs. Il s'agit ici de maintenir une autonomie d'usage pour la personne qui édite le contenu et une certaine autonomie technique en trouvant des personnes comprenant les outils techniques au niveau local.

La solution doit notamment permettre de manipuler l'arborescence des contenus par le biais d'une interface wysiwyg (what you see is what you get)

### Critère sécurité
La solution permettra d'accéder à un niveau de sécurité approprié aux collectivités, et à une traçabilité des contenus.

## Contraintes

À partir de ces critères fonctionnels, les choix techniques suivants ont été actés :

* utiliser le suivi de version par git pour la traçabilité et la sécurité du contenu ;
* servir le contenu par Hugo (meilleur générateur de site statique que nous avons identifié à ce jour) ;
* proposer un service aussi modulaire que possible pour faciliter sa réutilisation et sa maintenance.

## Benchmark

Ces différentes caractéristiques nous ont menés à réaliser le [benchmark](https://airtable.com/shrlSvwRn5bYQuUfa/tblBfy9buY1UpbKYQ) ci-dessous, afin de faciliter notre décision. Une version dotée de critères supplémentaires peut-être consultée via le lien précédent.

![Tableau Benchmark](img/ChoixEditeur-benchmark.png)

Aucune des solutions populaires ne répondait simplement à notre critère territorial. Celui-ci nous a semblé plus important que le critère de résilience axé sur la communauté.

Osuny nous apparaît alors être la solution la plus pertinente car :

* c'est un commun en devenir, avec un acteur français (Noesya, membre de COMENA - Commun Numérique en Nouvelle-Aquitaine) qui s'inscrit dans une vision commune, et avec lequel nous communiquons.
* les fonctionnalités proposées correspondent le mieux à nos besoins.
* le langage Ruby, utilisé par cette solution, mobilise une syntaxe simple, voire naturelle d'après son créateur, et est suffisamment résilient par le biais de sa communauté.

Cette solution ne remplit toutefois pas (encore) tous nos critères. La jeunesse de cet outil, maintenu par un nombre réduit de personnes, en fait un produit pour le moment peu documenté dont la communauté est restreinte. Cependant, la communauté des développeurs Ruby est suffisamment étoffée pour compenser ce point faible.

Cette décision prise, le développement de 2 fonctionnalités manquantes pour prototyper est lancé. La publication dans gitlab (auparavant seulement sur github) et l'ajout d'un nouveau profil (pour rendre invisibles des éléments spécifiques aux universités). Ces fonctionnalités permettront d'ancrer Osuny dans notre architecture d'édition et de publication de contenus, que l'on peut modéliser de la manière suivante :

![Architecture générale](img/ChoixEditeur-pipe.jpg)

## Lexique

##### MarkDown
Le [MarkDown](https://fr.wikipedia.org/wiki/Markdown) est une syntaxe de balisage légère pour mettre en forme du contenu. 

##### CMS
Content Management System. C'est un outil logiciel qui permet de produire et publier du contenu. WordPress, Drupal sont des CMS.

##### Headless CMS
C'est un outil logiciel qui permet seulement d'éditer le contenu et de l'exposer sur une API, sans le publier en HTML.

##### Flatfile CMS
C'est un CMS sans base de données, qui utilise uniquement des fichiers textes.

##### Git-based CMS
C'est un flatfile CMS qui utile git pour versionner son contenu.