# Exemple d'un journal de terrain

----

#### Résumé
Cet article présente un exemple d'un journal de terrain à Rocamadour

#### Dernière édition
06/2022

----

## 7 mars 2022

* Arrivée à Rocamadour
* Déjeuner avec l'équipe municipale
* Découverte du contexte de déploiement
	* Caractéristiques de la ville
	* Problématique du nom de domaine


### Ateliers

#### Cartographie des acteurs locaux

![Cartographie des acteurs locaux](img/JournalTerrain-carto.jpg)

Ce premier atelier consiste en une discussion libre de 30 minutes à 1 heure avec les élus et employés municipaux présents sur l'organisation générale de la commune et ses liens avec les autres acteurs locaux (département, région, préfecture, etc.). Ce premier temps permet d'établir une cartographie des relations avec chacun des acteurs identifiés.

#### Chapeaux de Bono

##### Déroulé de l'atelier
* Présentation de l'atelier et consignes
* Après présentation de l'atelier et de ses consignes, nous avons procédé à un temps d'introspection dédié à chacun des chapeaux mobilisé (Emotion, Optimisme, Pessimisme), à raison de 5 minutes par chapeau. Par la suite, nous avons animé un temps de restitution collectif des ressentis.

* Verbatims préliminaires
	* "Les gens n'ont pas connaissance des enjeux du numérique "
	* "Comme faciliter l'accès aux démarches administratives des habitants"

* Emotions
	* "Le site est à l'image de sa population, vieillissant. Il n'est pas accueillant pour les jeunes."
	* "Je suis dépitée, j'ai l'impression que personne n'y va, que mon travail ne sert pas à grand-chose [...] j'ai envie de transmettre"
	* "Je ressens de la frustration quand je compare notre sites à des sites de mairies plus beaux, plus agréables [...] on n'est pas trop écouté au conseil sur les enjeux numériques"
	* Ressentis globalement négatifs, l'interface est critiquée et le site actuel est peu utilisé. Il existe des difficultées à transmettre les informations par le biais de ce site, et à faire comprendre son importance auprès du conseil municipal.

* Optimisme
	* Le site est à jour en terme d'informations
	* Il existe une dynamique positive en interne. Malgré une mauvaise lisibilité, il est riche en informations

* Pessimisme
	* Le site est mal organisé"on s'y sent perdu". L'arborescence compte 5 points d'entrée dont 2 quasiment vides. Il est difficile à mettre à jour, son édition se révèle complexe.
	* "Le site n'est pas esthétique, il n'est pas adapté à ce que je ressens", comparaison explicite à*"Skyblog"*
	* "On peut pas avoir de stratégie [concernant les politiques autour du site web], on n'a pas les ressources"
	* mairie-rocamadour.fr et non rocamadour.fr rend le site difficile d'accès
	* Le réseau mobile sature en été, s'il n'y a pas forcément de difficultés d'accès à internet, c'est plus problématique pour les TPE (terminaux de paiement des marchants).

##### Synthèse

* Insatisfaction explicite sur l'état actuel du site web.
	* Interface considérée comme vieillissante et peu ergonomique
	* Arborescence complexe et mal structurée
* Contenus à jour pour les pages gérées par Nathalie, mais de nombreuses pages ont simplement fait l'objet de copié-collés à partir de l'ancien site.
* "Les gens nous font savoir qu'ils ne sont au courant de rien [...] on va dans d'autres communautés pour s'informer sur ce qui se passe à Rocamadour".


## 8 mars 2022

### Découverte de l'interface d'édition NEOPSE (back-office de Réseau des Communes)
* Assez semblable à Osuny, ce qui est une très bonne chose pour nous car cela facilitera la prise en main par les personnes en charges de saisir les contenus
* Fonctionnalités
	* Édition de la structure de la page
	* Gestionnaire de média
	* Prévisualisation de la page
	* Page builder WYSIWYG avec modules 1, 2, 3 colonnes
	* Fonctionnalités en surplus non utilisées (Billetterie, Agenda...)

### Échange téléphonique avec l'agence NET15 basée à Aurillac au sujet du nom de domainerocamadour.fr
* À vendre (> 100€ mais < 2000€) ils doivent nous communiquer un devis

### Réunion Syndicat Mixte
* Présents : Pierre BM, Timothée, Nathalie, Jean-Baptiste, Pierre Amaré, Nathalie, Céline Malignon (SM)
* Structure créée en 2007
* a mené à la question de l'attribution des compétences
	* [Proposition] : Que le nouveau site héberge une page sur qui porte quelle compétence
* La mairie possède le dernier mot sur les publications du site : Céline Malignon (notamment en charge du projet de Grand Site) transmet les contenus à Nathalie qui elle publie sur le site web, par exemple concernant les parkings (il y a 3 gestionnaires de parking à Roca)
	* Les informations sur les parkings ne sont sans doute pas les mêmes en fonction des profils : citoyens VS touriste
* Retours sur la question des usages
	* Informer les citoyens
	* Informer les touristes
	* Décrires les activités de la mairie
	* Médiation sur les compétences des structures
* On va avoir le fichier Illustrator de la carte de Roca, contenu clé du site (pas de licence posée mais libre pour Céline, directrice du syndic' mixte).
* Les navettes ont aussi été évoquées.

### Réunion Office du tourisme
* Présents : Pierre BM, Timothée, Jean-Baptiste, responsable marketing / SEO de l'OT
* 149 communes, pages dédiées sur une vingtaine de communes phares, dont Rocamadour
* une soixantaine de pages sur Rocamadour. Top pages Rocamadour
	* Page principale de présentation : 143 000 visites / an (2e page la plus visité de l'OT près celle de Lascaux)
	* Page infos pratiques (se garer, visiter, WC, quand venir...) : 26 000 visites / an
	* Se garer à Rocamadour : 15000 visites / an
	* Tarifs parking : 13 000 visites / an
	* Sanctuaire : 13 000 visites / an
	* Randonnées : 10 000 visites / an
* Stratégie SEO assez agressive, avec de nombreuses pages crées pas toutes accessible depuis l'arborescence (pour les intérêts économiques de l'OT). Référence 100% naturel selon leur dires.
* Data-tourisme ultra à jour, manque la brique fonctionnelle --> agenda automatisé + éventuelle partie manuelle : ajouté à la mairie tout ce qui a une portée touristique
* Visio à organiser avec Sébastien Mur(?), responsable de l'accueil : contact transmis à Jean-Baptiste
* [Action] : se mettre en lien avec le Syndicat Mixte pour essayer d'obtenir les infos de la place dispo sur les parkins en temps réel.

### Travail sur l'arborescence
* Présents : Gauthier, Pierre LR, Nathalie
* L'objectif de ce premier atelier était d'identifier des points d'entrée pour le site. Les enjeux de parcours de l'information non numériques ainsi les relations avec l'office du tourisme ont été des thèmes récurrents.
* Répartitions des questions liées au tourisme entre Rocamadour et l'office du tourisme
	* pour la mairie
		* Le stationnement (également rattaché aux enjeux de parcours non numériques)
		* La problématique du recrutement des saisonniers
			* Au-delà de leur recrutement se pose la question leur hébergement : cela n'est pas toujours considéré par les commerçants locaux-> possibilité de l'intégré dans le site ?
	* pour l'office du tourisme
		* Hébergement
		* Restauration
		* Attractions
* Question des démarches
	* Pacs, mariage, décès, naissances à Roca
	* Nombreux renvois vers des démarches / services d'état / d'administration
	* Maison France Service
	* Les navettes / le transport
* Découpage du site lié à la diplomatie interne, la répartition des taches, plus qu'à des fins d'expérience utilisateur
* Fusionner les différents annuaires via open data avec option de filtre ?

## 9 mars 2022

### Travail sur l'arborescence (suite)
* Poursuite du travail sur l'arborescence : présentation d'une première architecture du site et échanges avec l'équipe municipale (Dominique, Pierre, Jean-Baptiste, Nathalie) pour renommer les points d'entrées considérés et leur structuration
* Stabilisation d'une première version de l'arborescence avec Jean-Baptiste et Nathalie
	* Mairie
	* Vie locale
	* Démarches
	* Institutions
	* Tourisme
	* Actualités (hors nav)
	* Agenda (hors nav)
* Pierre BM fait des recherches sur les jurisprudences concernant la récupération de nom de domaine (rocamadour.fr) et envoyé un email à l'ANCT pour les tenir informés. Le protocole de l'AFNIC pour récupérer les noms de domaine est [ici](https://www.syreli.fr/fr/decisions/6).
