# Histoire

![Lalouvesc](img/Equipe-massiac.jpg)<br>
<i>Au tout début de Plateaux Numériques dans le Cantal en 2020</i><br>

[à compléter]


## Chronologie

* Janvier 2020, Gauhtier et Timothée commencent un travail de terrain en réalisant une enquête auprès des conseillers municipaux du village de Massiac, en Auvergne, sur leurs besoins.

* Début 2021 Suite à un financement européen nous avons pu déployer un premier pilote à Lalouvesc, village de 400 habitants dans le nord de l’Ardèche. Après 3 mois de travail, le site a été lancé en mars 2021 en collaboration proche avec les adjoints municipaux.

* Aout 2021 Plateaux numériques est un projet lauréat du plan France Relance, et est soutenu par l’Agence Nationale de la Cohésion des Territoires (ANCT)

* Fin 2021 Dépot du SIRET - Bertrand Keller (DevLead) et Pierre Bouvier-Muller (Coordination de projet) rejoignent le projet. Benoit Petit (DevOps) contribue à l'infrastructure.

* 31 janvier Pierre La Rocca rejoint l'équipe pour son stage de fin d'étude (UTC)

* 15 février Démarrage du projet à Rocamadour

* 24 février Anaëlle Beignon rejoint l'équipe pour travailler sur les parcours non-numériques

* 29 mars Léa Mosesso rejoint l'équipe dans le cadre de son master pour travailler sur les parcours non-numériques