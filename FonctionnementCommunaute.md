# Le fonctionnement de l'association

Le projet Plateaux Numériques est porté par une structure associative... Pourquoi ?

[à compléter]

## Intérêt général

Le projet Plateaux et ses membres sont concernés par l'intérêt commun et plus largement les transitions sociale (réduction des inégalités, des fractures technologiques), et environnementale (réduction de l'empreinte carbone).

L'association n'a donc pas vocation a générer de profit (but non lucratif) et a associer toutes les parties prenantes aux prises de décision.

## Gouvernance démocratique

[à compléter]

Conseil collégial

## Limiter les inégalités

Pas de but lucratif.

Pourquoi pas une SAS ?

[à compléter]