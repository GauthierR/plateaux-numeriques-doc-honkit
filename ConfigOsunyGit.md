# Configurer le lien entre Osuny et GitLab

----

#### Résumé
Cette fiche présente un tutoriel pour configurer la liaison entre Osuny et GitLab. Cette manipulation permet de mettre à jour automatique le contenu du site web.

#### Dernière édition
06/2022


#### État
Mise à jour nécessaire

----

## Liens d'accès :

Osuny : https://osuny.plateaux.tech/admin

GitLab (dit aussi Forge) : https://forge.liiib.re/


## 1 - Pour initialiser le dépôt avec la CI depuis GitLab

Aller sur : https://forge.liiib.re/plateaux-numeriques/infrastructure/-/blob/main/ansible/host_vars/localhost.yml

Sélectionner : Open in Web IDE

Ajouter un nom + commit

## 2 - Créer un dépôt sur GitLab

À partir de https://forge.liiib.re/dashboard/projects - créer un nouveau dépot

* Cliquer sur 'new project' puis sélectionner 'blank project'
	* Project name : entrez le nom de votre commune - vérifiez que le champ Project slug se met bien à jour concomitamment (il ne doit pas y avoir de majuscules, ni d'espaces, c'est normal)
	* Project URL, choisissez 'plateaux-numeriques' (plutôt que le nom de votre user - par défaut)
	* Visibility level : public

## 3 - Configurer l'accès API

*  Noter le Project ID, disponible sous le nom du projet

![image Project ID](img/ConfigOsunyGit-id.jpg)<br>

* Dans les réglages (Settings), configurer un Access Token doté des scopes (attention, il s'agit des réglages du compte gitlab, et non des réglages du projet)
	* api
	* read_user - [Pierre] je n'ai pas trouvé read user
	* read_api
	* read_repository
	* write_repository

Attention, il est nécessaire de copier le token avant de fermer la page.

## 4 - Créer un nouveau site web Osuny

* Aller dans le bandeau de gauche : Communication > Site web.
* Cliquer sur le bouton en bas à droite : 'créer'
* Renseigner les informations suivantes :
	* Informations
		* Nom : nom du site
		* URL : lien d'accès au site (non configurer pour le moment)
		* Sujet du site : Site indépendant (aucun sujet)

	* Git
		* Git provider : GitLab
		* Git endpoint : https://forge.liiib.re/api/v4
		* Access token : le token généré dans GitLab
		* Repository : le Project ID du dépot git

![Exemple de configuration fonctionnelle](img/ConfigOsunyGit-config.png)

Dans les versions récentes, Osuny propose de choisir la langue du site dans les paramètres d'édition. Nous vous conseillons de ne pas cocher ces derniers, car ils modifient la structure du site.

## 5 - Vérifier le fonctionnement

Une fois la configuration faite, la première modification du site Osuny publie automatiquement sur GitLab la structure du site.

### Remarques
Pour ajouter un contenu, il est nécessaire de cocher 'Publié ?'

