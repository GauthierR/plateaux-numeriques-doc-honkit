# Préparer le terrain

----

#### Résumé
Ce guide, à destination des déploiements de terrain, n'est pas exhaustif. N'hésitez-pas à y contribuer avec vos observations. Il peut être consulté en amont, ou pendant un terrain.

#### Dernière édition
07/2022

----

## Pourquoi un déploiement situé


[à remplir]

## Conseils et caractéristiques d'un déploiement situé

Chaque Plateau a vocation à être déployé dans une commune avec une histoire et des acteurs spécifiques. À ce titre, il est nécessaire de prendre en compte les caractéristiques propres à la communauté.

### Pourquoi aller sur place ?

* Aller sur place permet de tisser les premiers liens avec l'équipe municipale référente, à ce titre, il est recommandé d'être cordial et à l'écoute afin de créer une relation de confiance.

* Une relation de confiance réciproque est un plus non négligeable au travail de terrain, et aux taches à venir. Porter une attention particulière aux liens humains revient aussi à participer à la mise en place du site web, en facilitant les échanges à venir, en étendant le champ des actions possibles et en facilitant également la maintenance après déploiement.

### Durant le déploiement

* Tenir un journal de bord exhaustif sur les taches effectuées, pour documenter le processus de déploiement pour le référent, la communauté et les autres déploiements à venir.
	* Voir un exemple de journal de terrain [ici](JournalTerrain.md)
* Vous pouvez être amené à travailler sur des tâches qui ne concernent pas directement la conception du site internet. Prévoyez un temps supplémentaire dans le planning pour vous occuper de ce type d'activités durant votre séjour.
	* Ex : à Rocamadour, nous avons travaillé à la récupération du nom de domaine rocamadour.fr (sans succès)

### Remarques annexes

* Il est bien mieux d'aller dans une commune hors saison tourisitique, c'est à ce moment que les conseillers municipaux ont du temps pour travailler avec vous.

* Chaque région à ses produits particuliers, et vous serez sûrement amené à déjeuner avec l'équipe municipale sur place. N'hésitez pas à prévenir en avance si vous avez un régime alimentaire particulier.

## Préparer mon arrivée

### À préparer en amont

* S'informer sur le village de déploiement (nombre d'habitants, saison touristique, patrimoine et histoire...)

* Prendre contact avec l'équipe municipale pour gérer la logistique
	* Une commune dispose de gîtes municipaux peu utilisés hors saison, n'hésitez à demander si vous pouvez être logé là-bas
	* Prévoir l'itinéraire pour se rendre sur le lieu de déploiement

* Certains villages sont plutôt difficiles d'accès et le réseau ferré a ses limites. Il sera parfois nécessaire de louer une voiture ou autres. Ne préparez pas vos choix de transport au dernier moment

* Savoir quelles sont les options de connectivité sur place (lieu de travail et gîte) et prévoir si besoin d'utiliser le réseau 4G avec son téléphone.

### Suggestion de matériel à prévoir

#### Essentiels de vie

* Sac de couchage, drap house, taie d'oreiller
* Nécessaire de toilette
* Serviette de bain
* Vêtements adaptés à la saison

#### Essentiels de travail

* Matériel informatique
	* Câbles et chargeurs compris
* Appareil photo et batteries
* Matériel audio pour l'enregistrement si nécessaire
	* Veiller en amont à ce que les piles soient fonctionnelles, ou prévoir une batterie externe si le matériel utilisé est compatible
* Packs de Post-IT pour les ateliers
* Stylos / feutres et papeterie en général

#### Bonus

* Des produits de sa région à partager avec les acteurs locaux
