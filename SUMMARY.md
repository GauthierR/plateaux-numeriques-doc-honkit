# Summary

## Présentation
* [Bienvenue](README.md)
* [Contributeurs](Equipe.md)
* [Histoire](Histoire.md)

## Rejoindre la communauté
* [Fonctionnement](FonctionnementCommunaute.md)
* [Communiquer entre nous](Communiquer.md)


## Démarrer au niveau local
* [Trouver des villages intéressés]()
* [Trouver des aides financières et économiques]()
* [Rentrer en conformité](Conformite.md)
* [Préparer le terrain](PreparerTerrain.md)
* [Exemple de journal de terrain](JournalTerrain.md)
* [Comprendre le niveau de connectivité d'une commune]()
* [Éviter les contextes défavorables](ContexteDefavorable.md)

## Accompagner l'équipe municipale
* [Animer des ateliers](AnimerAteliers.md)
* [Comment rédiger des contenus accessibles](RedigerContenu.md)
* [Exemple d'arborescence-type](VoirArbo.md)

## Administrer le contenu d'un site
* [Présentation de l'interface d'édition](PresInterfaceEdition.md)
* [Créer un menu](CreerMenu.md)
* [Créer une page de contenu](CreerPageContenu.md)
* [Créer une actualité](CreerActu.md)

## Plonger dans la technique
* [Le choix de l'éditeur](ChoixEditeur.md)
* [Configurer le lien entre Osuny et Gitlab](ConfigOsunyGit.md)
* [Déployer un site dans la CI](DeployerCI.md)
* [CSS : les règles]()
* [Pipeline CI/CD]()
* [Gérer les noms de domaine](GererDomaines.md)

## Recherche en cours
* [Exclusion numérique](ParcoursNonNum.md)


