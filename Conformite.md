# Rentrer en conformité

----

#### Résumé
Cette page présente les mises en conformité auxquelles Plateaux Numériques permet de répondre : Règlement Général de Protection des Données, Règlement Général d'Amélioration de l'Accessibilité, Loi Lemaire (Open Data), Référentiel Général d'Ecoconception de Services Numériques.

#### Dernière édition
06/2022

----

## Les textes législatifs concernés

Cette page aborde les grandes lignes des lois relatives aux outils numériques et plus spécifiquement les directives auxquelles doivent se soumettre les collectivités. Les 4 grandes réglementations sur lesquelles nous portons notre attention sont présentées de manière chronologique, en fonction de leur date de publication. 

## Règlement Général de Protection des Données

Si vous souhaitez vous documenter sur le RGPD, nous pouvons recommander la lecture de la [page wikipedia](https://fr.wikipedia.org/wiki/R%C3%A8glement_g%C3%A9n%C3%A9ral_sur_la_protection_des_donn%C3%A9es) qui détaille les tenants et les aboutissants du RGPD.

Plateaux Numériques est conforme au RGPD par design puisseque le site ne connecte aucune données personnelles, ni n'installe aucun cookie. Ce sont des informations importantes qui doivent figurer dans les mentions légales.

Et si Matomo nous permet quand même d'avoir accès à quelques statistiques de consultation, c'est un service qui est hébergé par nos soins et dont aucun tiers n'a accès. 

### Définition

[à compléter]

## Règlement Général d'Amélioration de l'Accessibilité 

Les généralités sur le RGAA sont disponibles sur le [site du gouvernement](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/).

Plateaux Numériques est conforme au RGAA puisseque tous les thèmes disponibles sont audités (en général par Access42) et accompagnés d'une déclaration d'accessibilité. 

### Définition

[à compléter]

## Pour une république numérique (Loi Lemaire)

Elle a été promulguée le 7 octobre 2016 avec une série de dispositions en faveur de la circulation des données et du savoir (open data), la protection des citoyens et des consommateurs sur internet et l'accès au numérique dans tous les territoires et pour les publics en situation de handicap ou précaires.

Pour les collectivités, cette loi édicte des nouvelles règles par défaut: la publication des bases de données et des codes sources, la disponibilité des documents en ligne et la documentation des algorithmes et de leurs principales caractéristiques.

Ce n'est pas une loi **contraignante**, il n'y a pas de sanction applicable si elle n'est pas respectée. De plus, l'obligation de publier des données ouvertes ne s'applique qu'aux collectivités de plus de 3 500 habitants.

### Définition

Le 3 mai 2014, le [Journal Officiel](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000028890784) publie une définition des données ouvertes :les données qu'un organisme met à la disposition de tous sous forme de fichiers numériques afin de permettre leur réutilisation.

Il ne faut donc pas confondre données ouvertes et données publiques. Des entreprises privées peuvent également publier des données ouvertes.

Pour aller plus loin dans la compréhension de la Loi Lemaire, voir [ici](https://www.vie-publique.fr/eclairage/20301-loi-republique-numerique-7-octobre-2016-loi-lemaire-quels-changements).

## Référentiel Général d'Ecoconception de Services Numériques

[à compléter]

### Définition

[à compléter]