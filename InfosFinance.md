# Informations financières

## Subventions

* Agence Nationale de la Cohésion des Territoires
	* Projet sélectionné le 5 juillet 2021
	* Octroi acté le 28 juillet 2021
	* Subvention accordée par l'ANCT de 137 333€
	* Deux versements de 68 666€ au début et à la fin du projet
	* Information de facturation : Code service SFACT / Destinataire ANCT : SIRET 120 026 032 00016