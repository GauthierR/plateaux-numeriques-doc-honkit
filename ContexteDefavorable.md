# Éviter certaines erreurs (retour d'expérience)

Notre documentation héberge aussi les histoires de nos échecs, nos déconvenues. En voici une.

Vous le savez sans doute si vous êtes ici, le projet Plateaux Numériques a vocation à se déployer 'à la Tupperware', au travers de réseaux locaux, pour favoriser l'emploi, l'économie et les compétences territoriales.

Le premier réseau avec lequel nous avons eu de très bons contacts est anTIC Pays Basque une agence de développement créée par la communauté d'Agglomération Pays Basque, dont la mission est de défricher les nouvelles pratiques numériques pour les collectivités et de les accompagner de le déploiement.

Nous avions réfléchi avec l'anTIC a déployé un plateau dans une première ville mais l'équipe de l'agglomération a choisi un autre prestataire sans suivre la recommandation de l'anTIC. Ainsi, nous cherchions activement une autre commune pour lancer Plateaux numériques au pays basque avec l'anTIC. C'est finalement la ville de Boucau - avec laquelle travaille anTIC - qui avait vocation à accueillir le premier plateau avant de l'essaimer à d'autres collectivités.

Et malheureusement, les échanges avec la ville de Boucau n'ont pas abouti en notre faveur.

## Le contexte

Plateaux numériques ne pouvait pas travailler directement avec Boucau. Le projet de refonte de site web de la commune a été soumis à un appel à projet (que vous pourrez lire [ici](https://nuage.plateaux-numeriques.fr/s/3xpKzbtFnETqPWK)) auquel nous avons répondu. Notre réponse a été retenu avec deux autres offres pour une rencontre avec la directrice de la communication, l'élue en charge du numérique et la directrice générale des services.

## Un excès de confiance

Nous étions très (trop?) confiant dans notre capacité à être lauréat de ce projet car nous pensions remplir tous les critères, RGAA, empreinte environnementale, conception située.

Nous étions vraiment satisfait de la réponse que nous apportions, car elle semblait vraiment s'inscrire dans la bonne dynamique, avec le soutien de l'ANCT.

## Un design trop radical

C'est le point de critique principal, l'interface de [Lalouvesc](https://www.lalouvesc.fr/) n'a pas du tout plu aux interlocuteurs et interlocutrices. Et la critique est légitime, puisque c'était la commande qui avait été formulé par les élus locaux ardéchois, une interface épurée, proche des wiki qui ne fasse pas de fioriture (et respecte le RGAA).

Et bien que nous ayons partagé des projets ad hoc sur lequel nous avions pu travailler (par exemple le site de [Commown](https://commown.coop/)), cela n'a pas permis de rassurer les élus sur notre capacité à proposer une maquette plus conviviale, plus chaude.

L'appréciation de l'interface du premier plateau à Lalouvesc est bien évidemment propre à chaque personne. Des élus d'une autre commune trouvait la même interface plutôt conviviale. Néanmoins, la réaction à Boucau laisse penser qu'une variation graphique (et plus "chaude") de l'interface actuelle serait nécessaire pour convaincre un plus grand nombre d'élus.

## Une méthodologie incomprise

Tous nos projets se font au travers de l'enquête, de l'immersion sur place pour comprendre les défis, les enjeux auxquels sont confrontés la collectivité et les citoyens. Or, à Boucau, il a semblé que cette méthode suscite l'incompréhension.

Voici quels verbatims :
* "Les citoyens ne sont pas intéressés par le projet de refonte sur site internet".
* "Est-il envisageable de réaliser le projet sans que vous ne veniez faire cette enquête".

Ces 2 questions sont tout à fait légitimes et nous n'avons pas su y répondre correctement. Elles révèlent sans doute l'écart de perception, de compréhension des enjeux, d'habitudes différentes.

## Mettre trop de sujets sur la table

Faire un plateau permet de traiter la conformité RGAA, RGPD, RGESN et la loi Lemaire. De même, nous traitons des sujets d'inclusion numérique ou même de connectivité. Nous visons à répondre à beaucoup de sujets d'un coup avec un effort minimum.

Mais encore faut-il que l'interlocuteur sache que tous ces sujets existent et qu'il considère important de les traiter. Si l'interlocuteur en charge du dossier n'a pas tout cela en tête, une partie de la valeur de Plateaux numériques n'est pas visible et peut potentiellement lui desservir en mettant trop de sujets sur la table.

## À retenir

Si nous ne pouvons pas nous empêcher d'être déçus, nous apprenons que les enjeux de médiation doivent parfois être abordé dès les premiers contacts et la réponse aux appels d'offre.

Aussi, nous n'oublions pas que Plateaux Numériques a un caractère innovant, qui s'inscrit en contraste avec "le numérique c'est automatique" et qu'il sera sans doute plus facile de continuer les prototypes avec des 'early adopters'.