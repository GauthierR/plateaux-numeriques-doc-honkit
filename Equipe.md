# Équipe et contributeur·rice·s

## Association
* Gauthier Roussilhe, coordination
* Timothée Goguely, design et développement web

## Contributeur·rice·s
* Pierre Bouvier-Muller, référent local pour le Béarn
* Benoit Petit, DevOps
* Bertrand Keller, développement web et accessibilité
* Pierre La Rocca, couteau suisse
* Anaëlle Beignon, recherche utilisateur
* Léa Mosesso, recherche utilisateur
* Indiehosters, outils de travail libres
* Noesya, CMS
* Lysiane Lagadic, référente locale pour le Finistère (bientôt)

## Les ancien·ne·s

* Pierre Bouvier-Muller, coordinateur de projet
	* Du 17 janvier 2022 au 17 juillet 2022 (CDD 26h)
* Pierre La Rocca, design et développement
	* Du 31 janvier 2022 au 24 juillet 2022 (stage)


[à compléter]

## Souvenirs

![Lalouvesc](img/ReadMe-equipelalouvesc.jpg)<br>
<i>Avec l'équipe municipale de Lalouvesc en janvier 2021</i><br>

![Rocamadour](img/ReadMe-equiperoca.jpg)
<i>Avec l'équipe municipale de Rocamadour en mai 2022</i><br>

