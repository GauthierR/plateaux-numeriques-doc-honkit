# Gérer les noms de domaine

## Comment acheter un nom de domaine ?

[à remplir]

## Comment brancher le nom de domaine à un plateau ?

[à remplir]


## Comment récupérer un nom de domaine <nomdeville>.fr ?

### Contexte

Lors du déploiement du plateaux numérique à Rocamadour, nous avons identifié que le nom de domaine http://www.rocamadour.fr/ appartenait à une agence web https://www.net15.fr/ qui avait déployé à cette adresse quelques pages web assez simples.

![Page Rocamadour.fr](img/GererDomaines-roca.jpg)

Nous les avons contacté par téléphone afin de leur demander à quel point il était envisageable de récupérer ce nom de domaine.

La personne nous a indiqué que l'agence avait acquis de nombreux nom de domaine et que dans un intérêt économique, elle en faisait transaction.

Leur prix : 1 500€ Hors Taxe.

### Récupérer un nom de domaine

Votre nom de domaine (de commune) est détenu par un tiers ?

La loi XXXX Article 15 alinéa 2 (à confirmer) stipule :

Nous vous recommandons de prendre contact avec le détenteur du nom de domaine. Vous pouvez retrouver cette information via le WHOIS : https://whois.gandi.net/fr

Si la négociation à l'amiable ne fonctionne pas, il est possible d'utiliser le système de réglement des contentieux de l'AFNIC : https://www.syreli.fr/fr/. C'est une procédure qui demande de déposer un dossier et qui coûte 250€. Nous vous recommandons de prendre connaissance des précédentes décisions de l'AFNIC pour construire votre dossier.

#### Proposition de réponse à NET15 - Xavier Prillo

Bonjour Monsieur Prillo,

Nous préférons jouer carte sur table, la mairie de Rocamadour n'a malheureusement pas les moyens de payer 1500 euros pour acquérir le nom de domaine.

Après discussion, en interne, nous pouvons vous en proposer 200 euros, avec l'envie que [...]

### Analyse de l'ANCT

Nous avons transmis ces infos à Jean-Baptiste Jallet pour qu’il puisse en discuter au conseil municipal. On devrait pouvoir bénéficier de son expertise sur le plan juridique également.

Le site rocamadour.fr présente notamment les défaillances légales suivantes :

* Consentement RGPD
	* Google Analtyics et Addthis sont utilisés sur le site sans le consentement éclairé et explicite pour l’utilisateur.
	* Hors ces traceurs exportent des données utilisateurs et requièrent leur consentement (ex : popup sur les cookies)
		* https://www.cnil.fr/fr/cookies-et-traceurs-que-dit-la-loi
		* https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000037813978/

* Mentions légales
	* Les mentions obligatoires n’apparaissent pas ou manquent de détails.
	* https://www.economie.gouv.fr/entreprises/site-internet-mentions-obligatoires

* Préjudice d’image
	* Plusieurs fautes de français ternissent l’image de la ville à laquelle elle ne peut répondre, les informations sont obsolètes (notamment les commerces) ce qui désinforme les citoyens
	* L’agence n’explicite pas de manière évidente que ce site est commercial sans aucun lien consenti avec la ville.
	* Utilisation d'une marque à des fin d’utilisation d'espaces publicitaires et à caractère commercial :
		* http://www.rocamadour.fr/contact_fr.html : "N'hésitez pas à nous contacter si vous désirez apparaitre sur ce site”
		* "Sondage : Que souhaiteriez-vous de plus de la part de notre site ? De la publicité pour les petits producteurs locaux et petits entrepreneurs (28%, 34 votes)”
		* “Sondage : Que souhaiteriez-vous de plus de la part de notre site ? Une page pour des petite-annonces divers et variée (4%, 5 votes)"
	* Article L.45-2 3º du code des postes et des communications électroniques

* Pratique commerciale interdite
	* Présence de numéros surtaxés sans les mentions obligatoires : http://www.rocamadour.fr/article_15_1_boutique-iemanja_fr.html passible de 15 000€ d’amende
	* https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000032227057

* Parasitisme
	* Pratiques commerciales douteuse avec cybersquattage et négociation à un prix x100 de sa valeur sur le marché.
	* Jurisprudences : https://www.legifrance.gouv.fr/search/all?tab_selection=all&searchField=ALL&query=parasitisme&page=1&init=true
