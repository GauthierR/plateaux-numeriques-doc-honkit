# Exemple d'arboresence type

# Arborescence (mars 2022)

Première version de l'arborescence suite au travail effectué avec les équipes de la mairie de Rocamadour (Jean-Baptiste, Nathalie, Pierre Amaré, Dominique Lenfant) lors de la venue de PN début mars 2022.


![Arborescence](img/VoirArbo-postit.jpg)

---

**Section**

* Titre d'une page\
  *notes et remarques*
  * Contenu de la page

---

## Accueil

* Texte de bienvenue
* Accès rapides\
  *Raccourcis vers les 6 ou 7 pages les plus utiles / visitées*
* Actualités
* Agenda

## Mairie

* Informations pratiques
  * Adresse
  * Horaires d'ouverture
  * Contact
* Conseils municipaux et commissions
  * Conseillers
  * Commissions
  * Liste des procès verbaux par date
* Services administratifs et techniques
  * Services administratif
  * Service techniques
* Bulletins et lettres d'information
  * Bulletins (annuel)
  * Lettre d'information (trimestrielle)
* Plan local d'urbanisme (PLU)
  * *Accès au fichier téléchargeable du PLU*

## Vie locale

* La commune
  * Présentation
  * Situation géographique
  * Démographie
  * …
* Numéros utiles
  * Pompiers, gendarmerie, électricité, eau, violences
* Se déplacer
  * Abonnement parking
  * Ascenseur
  * Navette
  * Taxi
  * Plan
* Déchets et encombrants
  * Règles et horaires de collectes, coordonnées, Symictom
* Santé
  * Cabinet d'infirmiers
  * Maison de santé
  * EHPAD
  * Pharmacie
  * Hôpital
  * Maternité
* Agence postale communale
* Scolarité
  * Écoles (RPI)
  * Périscolaire
  * Ramassage scolaire
  * Collège
  * Lycée
* Associations
* Réservation des équipements municipaux
  * salle 1
  * salle 2
  * salle 3
  * cours de tenis
  * …
* Cimetières

## Démarches

*Reprise d'une partie de la terminologie des dépliants des Maison France Service (MFS) afin de faciliter la compréhension des habitant·e·s (une seule catégorisation des démarches à comprendre au lieu de deux). Au sein de chaque page, préciser si la démarche peut être effectuer à la mairie, en ligne, à la MFS ou auprès du Bus Cauvaldor. Ordre des pages à retravailler.*

* État civil et famille
  * Documents d'identité (CNI + passeport)
  * Naissance
  * PACS
  * Mariage
  * Décès
  * Livrets de famille
* Impôts
  * Taxe foncière
  * Lieu de perception
  * Lien impots.gouv
* Enfance
  * Carte scolaire
  * Transports
* Aides sociales
  * CAF
  * MSA
  * RSA
* Aides à la personne
  * Repas
  * Aide à domicile
  * Bracelet secours
* PV stationnement
  * Forfait post stationnement
  * Contestations
* Urbanisme
  * Demandes préalables
  * Permis de construire
  * Permis d'aménager
  * Autorisations de travaux
  * Réseaux (eau, fibre, etc.)
* Recensement
* Formation, emploi et retraites
* Carte grise
* Prévention santé
  * Carte vitale
  * Situation de handicap
* Logements, mobilité, courrier et service ADS (Lot habitat)
* Budget
  * Impôts
  * Difficultés financières
  * Litige de consommation
* Justice
  * Litige et conflit
  * Victime d'une infraction
* Commerce (ajout 04/05)
  * Réglementation de l'espace public et des façades commerciales
  * Tarif de l'affichage commercial
  * Accès au parking
  * Horaires de livraison

## Institutions

* Syndicat mixte et projet Grand Site
  * Présentation
  * Membres
  * Compétences
  * Action
  * Procès verbaux
* Communauté de communes Causses et vallée de la Dordogne (Cauvaldor)
  * Présentation
  * Compétences
* Partenaires institutionnels
  * Département du Lot
  * Région Occitanie
  * Services préfectoraux
* Parc Naturel Régional des Causses du Quercy

## Tourisme

* Office du tourisme
  * Infos, lien
* Se rendre, se garer et se déplacer à Rocamadour
  * Parking stationnement
  * Ascenseurs
  * navettes
* Carte
* Histoire & patrimoine
  * Terroir
  * Patrimoine culturel
  * Patrimoine religieux
* Randonnées
* Illuminations

---

Pages présentes et accessibles depuis la page d'accueil mais ne figurant pas comme menu de 1er niveau au sein de la navigation principale.

## Actualités

ajouter des catégories (ex: voirie et travaux) ?

## Agenda

* Évènements agrégés automatiquement via l'API data-tourisme utilisé par l'OT
* Évènements locaux complémentaires ajoutés manuellement par la mairie

---
