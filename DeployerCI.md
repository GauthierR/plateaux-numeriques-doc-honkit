# Déployer un site dans la CI

----

#### Résumé
Cette fiche présente un tutoriel pour configurer la liaison entre Osuny et GitLab. Cette manipulation permet de mettre à jour automatique le contenu du site web.

#### Dernière édition
06/2022

----

## Déploiement automatique

Cette manipulation permet de créer des sites fonctionnels, intégrant la CI, de manière automatique depuis gitlab, à partir d'un template starter-kit.

* Se rendre sur la page suivante : https://forge.liiib.re/plateaux-numeriques/infrastructure/-/blob/main/ansible/host_vars/localhost.yml

* Ajouter les lignes suivantes en respectant le formalisme .yml. Attention aux indentations.

```
- name: nom-site-a-creer
      state: present
```

* Commiter l'ajout effectué

* Le site nouvellement créé est disponible dans le dossier mairie.

## Ajout d'une CI à un répertoire Git déjà existant

Ce option technique permet d'ajouter manuellement une CI sur un répertoire Gitlab déjà existant et lier à Osuny. Il est recommandé d'avoir une connaissance intermédiaire de Git.

* Cloner le repos en local
* Récupérer d'un répertoire existant et configuré pour la CI

```
les fichiers suivants :
go.mod
wait*_*for_connection.sh
.gitlab-ci.yml

les dossiers suivants : 
config
thems
deploy
```
* Modifier en remplaçant par le nom du répertoire les fichier

```
config/production/config.yaml
data/website.yml
```

Démarche complémentaire : effectuer via un terminal un grep -R nomdurepertoiresdontonaclonélaci pour vérifier qu'il ne reste plus de traces de l'ancien répos.

* Effectuer une publication depuis Osuny pour lancer la CI