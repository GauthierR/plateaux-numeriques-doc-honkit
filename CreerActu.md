# Création d'actualités

----

#### Résumé
Cette page présente les différentes possibilités de l'interface d'édition d'actualités depuis Osuny.

#### Dernière édition
06/2022

----

## Liens d'accès

Pour accèder à l'interface d'édition Osuny : https://edition.plateaux.tech

Puis, se rendre dans l'onglet "Actualités".

### Avant-Propos

Pour une présentation générale de l'onglet des actualités, veuillez-vous référer à l'article sur [l'interface d'édition](PresInterfaceEdition.md).

## Créer une actualité

![Actualité](img/PresInterfaceEdition-actualites.png)

Pour créer une actualité, depuis l'onglet "Actualités", appuyer sur "Créer", en 1, en bas à droite de l'écran.

[Note : à continuer lorsque la page "Actualités" sera fonctionnelle pour l'ajout de contenu]

## Créer une catégorie

Les catégories permettent de trier les actualités par thématique. (ex : évènement local, annonce municipale...)

Pour créer une catégorie, depuis l'onglet "Actualités", appuyer sur "Créer", en 3, dans la zone Catégorie.

![Catégorie](img/CreerActu-categorie.png)

Une fois les données saisies et enregistrées, la catégorie apparaîtra dans la page Actualités.