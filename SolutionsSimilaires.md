# Benchmark

## Campagnol

Campagnol a été créé en 2010 à l'initiative de l'association des maires ruraux de France (AMRF) dont les adhérents avaient émis le besoin d'un outil pour créer des sites web rapidement, informatifs et bon marché pour les collectivités rurales (moins de 3500 habitants).

Campagnol V1 est donc lancé en 2010 sous SPIP avec une subvention de 30 000€. "L'outil est développé sur un noyau libre Spip dont l'AMRF a acquis la licence. Il est conforme au référentiel d'accessibilité pour les administrations (RGAA), gage d'ouverture vers les personnes handicapées. Et bien sûr, il est pratiquement prêt pour le paiement en ligne, ADeP oblige ! L'investissement pour son développement a d'ailleurs bénéficié d'une subvention de la région Rhône-Alpes à hauteur de 30 000 euros. "Je ne fais que le répéter : sous réserve d'accès au haut débit, voire au très haut débit, les élus ruraux ne sont pas hostiles aux TIC !", [a conclu le président Vanik Berberian](https://www.banquedesterritoires.fr/lamrf-propose-un-service-par-abonnement-de-creation-de-sites-web-communaux)" 

Campagnol V2 est maintenant un thème WP lancé en 2018. Le coût de développement de cette nouvelle version est inconnu. Au sein de l'AMRF, deux personnes semblent s'occuper de la gestion de Campagnol et de la relation client avec les "clients".

### Ressources
* [Site web](https://campagnol.fr/)
* [Film de présentation de Campagnol](https://www.youtube.com/watch?v=Za6KTK7I9Xo&t=214s) 

## Réseau des communes

Réseau des communes a commencé son activité en 2008, et prend aujourd'hui la forme d'une SASU avec 3 à 5 employés. Les sites récents sont créés sur un moteur appelé Neopse qui a été rachété par le Réseau des Communes en 2018. Les technologies utilisées par Neopse sont inconnues.

Cet acteur semble équiper plus de 1000 collectivités. Son chiffre d'affaires en 2019 était de 885 000€, avec une perte de 52 000€.

### Ressources
* [Site web](https://reseaudescommunes.fr/)

## Cmonsite

"CmonSite est un outil qui vous offrira un site web moderne pensé pour vos utilisateurs. Afin de vous proposer le meilleur outil possible, nos équipes se sont mobilisées et ont travaillé sur une solution clé en main qui vous permette de générer un site à votre image et qui soit à la fois sécurisé et adaptatif pour tous les écrans".

La solution repose sur Drupal CMS et génère des sites statiques avec Gatsby.


### Ressources
* [Site web](https://docs.ternum-bfc.fr/cmonsite)
* [Film de présentation](https://vimeo.com/462608472?embedded=true&source=vimeo_logo&owner=4115716)

## Applications
* [Panneau Pocket](https://www.panneaupocket.com/) (soutenu par AMRF)
* [Intramuros](https://appli-intramuros.fr/)
* [CityAll](https://play.google.com/store/apps/details?id=com.lumiplan.city.wall&hl=fr&gl=US)