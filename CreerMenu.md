# Créer un menu

----

#### Résumé
Cette page présente l'interface d'édition de menus avec Osuny.

#### Dernière édition
06/2022

----

## Liens d'accès

Pour accèder à l'interface d'édition Osuny : https://edition.plateaux.tech

Puis, se rendre dans l'onglet "Menu", cliquer sur le nom du menu à éditer (Menu Principal ou Menu Rapide).

## Présentation de l'interface

![Menu principal](img/CreerMenu-menuprincipal.png)

L'illustration ci-dessus présente les éléments par défaut du Menu Pricipal. Nous vous conseillons de ne pas les supprimer.

On ajoute dans les menus les points d'accès généraux de navigation. Il est conseillé d'éviter la création de plusieurs niveaux de hiérarchie dans les menus, non pris en charge par le thème à ce jour. Pour mettre en place une hiérarchie entre les pages, nous vous invitons à définir celle-ci directement dans les pages. Pour plus d'informations, vous pouvez consulter la page sur la [création de pages dans l'éditeur](CreerPageContenu.md).

## Créer un élément de menu

Cliquer sur le bouton "Créer" pour se rendre dans l'interface de création d'un élément de menu. Une fois sa création terminée, l'élément apparaîtra directement dans l'arborescence du menu.

![Nouvel élément](img/CreerMenu-elementmenu.png)

Plusieurs options sont disponibles :

* Titre intermédiaire : définir un titre intermédiaire dans le menu. Ce titre est un élément informatif.
* URL : définir un lien d'accès vers une page web extérieure au site. (ex : le site de la communauté de communes).
* Page Spécifique : définir une entrée vers une page du site. En choisissant la Cible avant le Titre, le titre affiché dans le Menu sera celui de la page cible.
* Catégorie d'actualités : définir une entrée vers une catégorie d'actualités. Pour en savoir plus sur les catégories d'actualités, vous pouvez consulter la page sur les [actualités](CreerActu.md).

## Créer un sous-élément de menu

Cliquer sur le nom d'un élément du menu pour accéder à l'interface d'ajout de sous-éléments.

![Nouvel élément](img/CreerMenu-souselementmenu.png)

Dans les informations apparaît le type de page de l'entrée de menu "mère", ainsi que son nom. Dans l'illustration ci-dessus, on ajoute un sous-élément à la Page spécifique Démarches.

Appuyer sur le bouton "Créer" pour accéder à une interface similaire à celle présentée dans la section "Créer un élément de menu".